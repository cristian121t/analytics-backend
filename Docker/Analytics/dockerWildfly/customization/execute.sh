#!/bin/bash

# Usage: execute.sh [WildFly mode] [configuration file]
#
# The default mode is 'standalone' and default configuration is based on the
# mode. It can be 'standalone.xml', or 'domain.xml'.

JBOSS_HOME=/opt/jboss/wildfly
JBOSS_CLI=$JBOSS_HOME/bin/jboss-cli.sh
JBOSS_MODE=${1:-"standalone"}
JBOSS_CONFIG=${2:-"$JBOSS_MODE-full.xml"}

function wait_for_server() {
  until `$JBOSS_CLI -c ":read-attribute(name=server-state)" 2> /dev/null | grep -q running`; do
    sleep 1
  done
}

echo "=> Starting WildFly server"
$JBOSS_HOME/bin/$JBOSS_MODE.sh -b 0.0.0.0 -bmanagement 0.0.0.0 -c $JBOSS_CONFIG &

echo "=> Waiting for the server to boot"
wait_for_server

echo "=> Executing the commands"

echo "=> POSTGRESQL_URI (docker with networking): " $POSTGRESQL_URI
echo "=> POSTGRES_DB (explicit): " $POSTGRES_DB

# Adding modules and datasources
echo "=> Adding modules and datasources"
$JBOSS_CLI --file=/opt/jboss/wildfly/customization/add-datasources.cli -DJBOSS_HOME=$JBOSS_HOME -DPOSTGRESQL_URI=$POSTGRESQL_URI -DPOSTGRES_DB=$POSTGRES_DB -DPOSTGRES_USER=$POSTGRES_USER -DPOSTGRES_PASSWORD=$POSTGRES_PASSWORD -DJBOSS_DATASOURCE_NAME=$JBOSS_DATASOURCE_NAME -DJBOSS_JNDI_DATASOURCE_NAME=$JBOSS_JNDI_DATASOURCE_NAME

# Deploy the WAR
cp /opt/jboss/wildfly/customization/Analytics-1.0.war $JBOSS_HOME/$JBOSS_MODE/deployments/Analytics-1.0.war

# Add admin user
echo "=> Adding new wildfly user"
$JBOSS_HOME/bin/add-user.sh $JBOSS_ADMIN_USER $JBOSS_ADMIN_PASSWORD --silent

echo "=> Shutting down WildFly"
if [ "$JBOSS_MODE" = "standalone" ]; then
  $JBOSS_CLI -c ":shutdown"
else
  $JBOSS_CLI -c "/host=*:shutdown"
fi

export JAVA_OPTS="$JBOSS_JAVA_OPTS -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true"

echo "=> SET JAVA OPTIONS: " $JAVA_OPTS

echo "=> Restarting WildFly"
$JBOSS_HOME/bin/$JBOSS_MODE.sh -b 0.0.0.0 -bmanagement 0.0.0.0 -c $JBOSS_CONFIG

echo "=> Waiting for the server to reboot"
#wait_for_server

echo "=> WildFly Restarted"
