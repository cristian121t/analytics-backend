/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.persistence;

import com.ameise.creditcardbalancer.model.commerce_trade;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class commerce_tradeFacade extends AbstractFacade<commerce_trade> {

    @PersistenceContext(unitName = "Analytics_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public commerce_tradeFacade() {
        super(commerce_trade.class);
    }
    
}
