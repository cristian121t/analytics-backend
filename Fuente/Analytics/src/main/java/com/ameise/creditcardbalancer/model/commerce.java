package com.ameise.creditcardbalancer.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "COMMERCE", schema = "public")
@XmlRootElement
public class commerce implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "COM_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int com_id;

    @Column(name = "COM_NAME", length = 50)
    private String com_name;

    @Column(name = "COM_DESCRIPTION", length = 250)
    private String com_description;

    @Column(name = "COM_ADDRESS", length = 150)
    private String com_address;

    @Column(name = "COM_EMAIL", length = 150)
    private String com_email;

    @Column(name = "COM_CONTACT", length = 150)
    private String com_contact;

    @Column(name = "COM_STATUS", length = 3)
    private String com_status;

    @JoinColumn(name = "COT_ID")
    @ManyToOne(cascade = {CascadeType.ALL}, targetEntity = commerce_trade.class, optional = false)
    private commerce_trade commerce_trade;

    public int getCom_id() {
        return com_id;
    }

    public void setCom_id(int com_id) {
        this.com_id = com_id;
    }

    public String getCom_name() {
        return com_name;
    }

    public void setCom_name(String com_name) {
        this.com_name = com_name;
    }

    public String getCom_description() {
        return com_description;
    }

    public void setCom_description(String com_description) {
        this.com_description = com_description;
    }

    public String getCom_address() {
        return com_address;
    }

    public void setCom_address(String com_address) {
        this.com_address = com_address;
    }

    public String getCom_email() {
        return com_email;
    }

    public void setCom_email(String com_email) {
        this.com_email = com_email;
    }

    public String getCom_contact() {
        return com_contact;
    }

    public void setCom_contact(String com_contact) {
        this.com_contact = com_contact;
    }

    public String getCom_status() {
        return com_status;
    }

    public void setCom_status(String com_status) {
        this.com_status = com_status;
    }

    public commerce_trade getCommerce_trade() {
        return commerce_trade;
    }

    public void setCommerce_trade(commerce_trade commerce_trade) {
        this.commerce_trade = commerce_trade;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.com_id;
        hash = 37 * hash + Objects.hashCode(this.com_name);
        hash = 37 * hash + Objects.hashCode(this.com_description);
        hash = 37 * hash + Objects.hashCode(this.com_address);
        hash = 37 * hash + Objects.hashCode(this.com_email);
        hash = 37 * hash + Objects.hashCode(this.com_contact);
        hash = 37 * hash + Objects.hashCode(this.com_status);
        hash = 37 * hash + Objects.hashCode(this.commerce_trade);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final commerce other = (commerce) obj;
        if (this.com_id != other.com_id) {
            return false;
        }
        if (!Objects.equals(this.com_name, other.com_name)) {
            return false;
        }
        if (!Objects.equals(this.com_description, other.com_description)) {
            return false;
        }
        if (!Objects.equals(this.com_address, other.com_address)) {
            return false;
        }
        if (!Objects.equals(this.com_email, other.com_email)) {
            return false;
        }
        if (!Objects.equals(this.com_contact, other.com_contact)) {
            return false;
        }
        if (!Objects.equals(this.com_status, other.com_status)) {
            return false;
        }
        if (!Objects.equals(this.commerce_trade, other.commerce_trade)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "commerce{" + "com_id=" + com_id + ", com_name=" + com_name + ", com_description=" + com_description + ", com_address=" + com_address + ", com_email=" + com_email + ", com_contact=" + com_contact + ", com_status=" + com_status + ", commerce_trade=" + commerce_trade + '}';
    }

}
