/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.persistence;

import com.ameise.creditcardbalancer.model.transactions;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class transactionsFacade extends AbstractFacade<transactions> {

    @PersistenceContext(unitName = "Analytics_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public transactionsFacade() {
        super(transactions.class);
    }
    
}
