/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author andres
 */
@Entity
@Table(name = "PRODUCT", schema = "public")
@XmlRootElement
public class product implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PRO_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int pro_id;

    @Column(name = "PRO_NAME", length = 150)
    private String pro_name;

    @Column(name = "PRO_TYPE", length = 150)
    private String pro_type;

    @Column(name = "PRO_TRADE", length = 150)
    private String pro_trade;

    @Column(name = "PRO_TRADEDETAILS", length = 250)
    private String pro_tradeDetails;

    public int getPro_id() {
        return pro_id;
    }

    public void setPro_id(int pro_id) {
        this.pro_id = pro_id;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getPro_type() {
        return pro_type;
    }

    public void setPro_type(String pro_type) {
        this.pro_type = pro_type;
    }

    public String getPro_trade() {
        return pro_trade;
    }

    public void setPro_trade(String pro_trade) {
        this.pro_trade = pro_trade;
    }

    public String getPro_tradeDetails() {
        return pro_tradeDetails;
    }

    public void setPro_tradeDetails(String pro_tradeDetails) {
        this.pro_tradeDetails = pro_tradeDetails;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.pro_id;
        hash = 79 * hash + Objects.hashCode(this.pro_name);
        hash = 79 * hash + Objects.hashCode(this.pro_type);
        hash = 79 * hash + Objects.hashCode(this.pro_trade);
        hash = 79 * hash + Objects.hashCode(this.pro_tradeDetails);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final product other = (product) obj;
        if (this.pro_id != other.pro_id) {
            return false;
        }
        if (!Objects.equals(this.pro_name, other.pro_name)) {
            return false;
        }
        if (!Objects.equals(this.pro_type, other.pro_type)) {
            return false;
        }
        if (!Objects.equals(this.pro_trade, other.pro_trade)) {
            return false;
        }
        if (!Objects.equals(this.pro_tradeDetails, other.pro_tradeDetails)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "product{" + "pro_id=" + pro_id + ", pro_name=" + pro_name + ", pro_type=" + pro_type + ", pro_trade=" + pro_trade + ", pro_tradeDetails=" + pro_tradeDetails + '}';
    }

}
