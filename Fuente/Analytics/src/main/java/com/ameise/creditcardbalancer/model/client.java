package com.ameise.creditcardbalancer.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CLIENT", schema = "public")
@XmlRootElement
public class client implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CLI_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cli_id;

    @Column(name = "CLI_IDENTIFICATION", length = 13)
    private String cli_identification;

    @Column(name = "CLI_NAME", length = 50)
    private String cli_name;

    @Column(name = "CLI_CORTE", length = 2)
    private int cli_corte;

    @Column(name = "CLI_ADDRESS", length = 150)
    private String cli_address;

    @Column(name = "CLI_EMAIL", length = 150)
    private String cli_email;

    @Column(name = "CLI_CONTACT", length = 150)
    private String cli_contact;

    @Column(name = "CLI_STATUS", length = 3)
    private String cli_status;

    @Column(name = "CLI_VALUES_SESSION", length = 250)
    private String cli_values_session;

    public int getCli_id() {
        return cli_id;
    }

    public void setCli_id(int cli_id) {
        this.cli_id = cli_id;
    }

    public String getCli_identification() {
        return cli_identification;
    }

    public void setCli_identification(String cli_identification) {
        this.cli_identification = cli_identification;
    }

    public String getCli_name() {
        return cli_name;
    }

    public void setCli_name(String cli_name) {
        this.cli_name = cli_name;
    }

    public int getCli_corte() {
        return cli_corte;
    }

    public void setCli_corte(int cli_corte) {
        this.cli_corte = cli_corte;
    }

    public String getCli_address() {
        return cli_address;
    }

    public void setCli_address(String cli_address) {
        this.cli_address = cli_address;
    }

    public String getCli_email() {
        return cli_email;
    }

    public void setCli_email(String cli_email) {
        this.cli_email = cli_email;
    }

    public String getCli_contact() {
        return cli_contact;
    }

    public void setCli_contact(String cli_contact) {
        this.cli_contact = cli_contact;
    }

    public String getCli_status() {
        return cli_status;
    }

    public void setCli_status(String cli_status) {
        this.cli_status = cli_status;
    }

    public String getCli_values_session() {
        return cli_values_session;
    }

    public void setCli_values_session(String cli_values_session) {
        this.cli_values_session = cli_values_session;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.cli_id;
        hash = 83 * hash + Objects.hashCode(this.cli_identification);
        hash = 83 * hash + Objects.hashCode(this.cli_name);
        hash = 83 * hash + this.cli_corte;
        hash = 83 * hash + Objects.hashCode(this.cli_address);
        hash = 83 * hash + Objects.hashCode(this.cli_email);
        hash = 83 * hash + Objects.hashCode(this.cli_contact);
        hash = 83 * hash + Objects.hashCode(this.cli_status);
        hash = 83 * hash + Objects.hashCode(this.cli_values_session);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final client other = (client) obj;
        if (this.cli_id != other.cli_id) {
            return false;
        }
        if (this.cli_corte != other.cli_corte) {
            return false;
        }
        if (!Objects.equals(this.cli_identification, other.cli_identification)) {
            return false;
        }
        if (!Objects.equals(this.cli_name, other.cli_name)) {
            return false;
        }
        if (!Objects.equals(this.cli_address, other.cli_address)) {
            return false;
        }
        if (!Objects.equals(this.cli_email, other.cli_email)) {
            return false;
        }
        if (!Objects.equals(this.cli_contact, other.cli_contact)) {
            return false;
        }
        if (!Objects.equals(this.cli_status, other.cli_status)) {
            return false;
        }
        if (!Objects.equals(this.cli_values_session, other.cli_values_session)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "client{" + "cli_id=" + cli_id + ", cli_identification=" + cli_identification + ", cli_name=" + cli_name + ", cli_corte=" + cli_corte + ", cli_address=" + cli_address + ", cli_email=" + cli_email + ", cli_contact=" + cli_contact + ", cli_status=" + cli_status + ", cli_values_session=" + cli_values_session + '}';
    }

}
