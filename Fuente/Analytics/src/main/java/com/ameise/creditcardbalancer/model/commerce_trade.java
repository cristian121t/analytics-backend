package com.ameise.creditcardbalancer.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "COMMERCE_TRADE", schema = "public")
@XmlRootElement
public class commerce_trade implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "COT_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cot_id;

    @Column(name = "COT_NAME", length = 50)
    private String cot_name;

    @Column(name = "COT_DESCRIPTION", length = 250)
    private String cot_description;

    public int getCot_id() {
        return cot_id;
    }

    public void setCot_id(int cot_id) {
        this.cot_id = cot_id;
    }

    public String getCot_name() {
        return cot_name;
    }

    public void setCot_name(String cot_name) {
        this.cot_name = cot_name;
    }

    public String getCot_description() {
        return cot_description;
    }

    public void setCot_description(String cot_description) {
        this.cot_description = cot_description;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + this.cot_id;
        hash = 59 * hash + Objects.hashCode(this.cot_name);
        hash = 59 * hash + Objects.hashCode(this.cot_description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final commerce_trade other = (commerce_trade) obj;
        if (this.cot_id != other.cot_id) {
            return false;
        }
        if (!Objects.equals(this.cot_name, other.cot_name)) {
            return false;
        }
        if (!Objects.equals(this.cot_description, other.cot_description)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "commerce_trade{" + "cot_id=" + cot_id + ", cot_name=" + cot_name + ", cot_description=" + cot_description + '}';
    }

}
