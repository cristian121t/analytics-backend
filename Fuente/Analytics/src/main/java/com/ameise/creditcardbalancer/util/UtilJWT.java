/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.util;

import com.ameise.creditcardbalancer.persistence.clientFacade;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 */
@Stateless
public class UtilJWT implements Serializable {

    private static final Logger LOG = Logger.getLogger(UtilJWT.class.getName());

    @Inject
    private clientFacade ejbFacade;

    public String createToken(String identification) {
        try {
            if (ejbFacade.findClient(identification) != null) {
                Algorithm algorithm = Algorithm.HMAC256(identification);
                return "{\"token\":\"" + JWT.create().withIssuer("auth0").sign(algorithm) + "\"}";
            } else {
                LOG.error("Cliente no registrado: createToken[" + identification + "]");
                return "{\"codResp\":\"1001\",\"msjResp\":\"Cliente no registrado\"}";
            }
        } catch (UnsupportedEncodingException | JWTCreationException ex) {
            LOG.error("Error en la obtencion del token: createToken[" + identification + "]");
            LOG.error(ex);
            return "{\"codResp\":\"1002\",\"msjResp\":\"Error en la obtencion del token\"}";
        }
    }

    public boolean validationToken(String token, String identification) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(identification);
            JWTVerifier verifier = JWT.require(algorithm).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            LOG.info("Validacion de token exitosa: validationToken[" + token + "," + identification + "]");
            return true;
        } catch (UnsupportedEncodingException | JWTVerificationException ex) {
            LOG.error("Error en la validacion del token: validationToken[" + token + "," + identification + "]");
            LOG.error(ex);
            return false;
        }
    }

}
