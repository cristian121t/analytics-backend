/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.persistence;

import com.ameise.creditcardbalancer.model.product;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author andres
 */
@Stateless
public class productFacade extends AbstractFacade<product> {

    @PersistenceContext(unitName = "Analytics_PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public productFacade() {
        super(product.class);
    }
    
}
