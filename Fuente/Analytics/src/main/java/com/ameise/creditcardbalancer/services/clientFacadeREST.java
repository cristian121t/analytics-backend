/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.services;

import com.ameise.creditcardbalancer.model.client;
import com.ameise.creditcardbalancer.util.UtilJWT;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andres
 */
@Stateless
@Path("clients")
public class clientFacadeREST extends AbstractFacade<client> {

    private static final Logger LOG = Logger.getLogger(clientFacadeREST.class.getName());

    @PersistenceContext(unitName = "Analytics_PU")
    private EntityManager em;

    @Inject
    private UtilJWT jwt;

    public clientFacadeREST() {
        super(client.class);
    }

    @POST
    @OPTIONS
    @Path("getHeader")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findHeader(@Context HttpHeaders headers, String clientDATA) {
        LOG.info("Nueva peticion realizada: >>  User-Agent: " + headers.getRequestHeader("user-agent").get(0) + " << >>  Data: " + clientDATA);
        JSONObject JSONObject = new JSONObject(clientDATA);
        String token = JSONObject.getString("token");
        String identification = JSONObject.getString("identification");
        if (jwt.validationToken(token, identification)) {
            return Response.ok(super.findClient(identification))
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        } else {
            return Response.ok("{\"codResp\":\"1003\"\"msjResp\":\"Error en la validacion de token\"}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        }

    }

    @POST
    @OPTIONS
    @Path("getToken")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findToken(@Context HttpHeaders headers, String clientDATA) {
        LOG.info("Nueva peticion realizada: >> User-Agent: " + headers.getRequestHeader("user-agent").get(0) + " << >>  Data: " + clientDATA);
        JSONObject JSONObject = new JSONObject(clientDATA);
        String identification = JSONObject.getString("identification");
        return Response.ok(jwt.createToken(identification))
                .header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                .build();
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
