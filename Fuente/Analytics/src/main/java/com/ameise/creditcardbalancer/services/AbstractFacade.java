/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.services;

import com.ameise.creditcardbalancer.model.channel;
import com.ameise.creditcardbalancer.model.client;
import com.ameise.creditcardbalancer.model.commerce;
import com.ameise.creditcardbalancer.model.commerce_trade;
import com.ameise.creditcardbalancer.model.product;
import com.ameise.creditcardbalancer.model.transactions;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.criteria.Expression;
import org.apache.log4j.Logger;

/**
 *
 * @author andres
 * @param <T>
 */
public abstract class AbstractFacade<T> {

    private static final Logger LOG = Logger.getLogger(AbstractFacade.class.getName());

    private final Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

///////////////////////////////////////////////////////////////////////////////////////////////////
//      
///////////////////////////////////////////////////////////////////////////////////////////////////
    public transactions findTransaction(Integer tran_id) {
        javax.persistence.criteria.CriteriaQuery transactionQuery = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> transactionAttribute = transactionQuery.from(transactions.class);
        transactionQuery.select(transactionAttribute);
        transactionQuery.where(getEntityManager().getCriteriaBuilder().equal(transactionAttribute.get("tra_id").as(Integer.class), tran_id));
        try {
            return (transactions) getEntityManager().createQuery(transactionQuery).getSingleResult();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public Integer findTransactionID() {
        javax.persistence.criteria.CriteriaQuery transactionQuery = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> transactionAttribute = transactionQuery.from(transactions.class);
        Expression<Integer> MaxExp = getEntityManager().getCriteriaBuilder().max(transactionAttribute.get("tra_id").as(Integer.class));
        transactionQuery.select(MaxExp);
        try {
            return (Integer) getEntityManager().createQuery(transactionQuery).getSingleResult();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public client findClient(String identification) {
        javax.persistence.criteria.CriteriaQuery clietQuery = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> clientAttribute = clietQuery.from(client.class);
        clietQuery.select(clientAttribute);
        clietQuery.where(getEntityManager().getCriteriaBuilder().like(clientAttribute.get("cli_identification").as(String.class), identification));
        try {
            return (client) getEntityManager().createQuery(clietQuery).getSingleResult();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public channel findChannel(String name) {
        javax.persistence.criteria.CriteriaQuery Query = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> Attribute = Query.from(channel.class);
        Query.select(Attribute);
        Query.where(getEntityManager().getCriteriaBuilder().like(Attribute.get("cha_name").as(String.class), name));
        try {
            return (channel) getEntityManager().createQuery(Query).getSingleResult();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public commerce findCommerce(String name) {
        javax.persistence.criteria.CriteriaQuery Query = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> Attribute = Query.from(commerce.class);
        Query.select(Attribute);
        Query.where(getEntityManager().getCriteriaBuilder().like(Attribute.get("com_name").as(String.class), name));
        try {
            return (commerce) getEntityManager().createQuery(Query).getSingleResult();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public commerce_trade findCommerceTrade(String name) {
        javax.persistence.criteria.CriteriaQuery Query = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> Attribute = Query.from(commerce_trade.class);
        Query.select(Attribute);
        Query.where(getEntityManager().getCriteriaBuilder().like(Attribute.get("cot_name").as(String.class), name));
        try {
            return (commerce_trade) getEntityManager().createQuery(Query).getSingleResult();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public product findProduct(String name, String tradeDetails) {
        javax.persistence.criteria.CriteriaQuery Query = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> Attribute = Query.from(product.class);
        Query.select(Attribute);
        Query.where(getEntityManager().getCriteriaBuilder().like(Attribute.get("pro_name").as(String.class), name),
                getEntityManager().getCriteriaBuilder().and(getEntityManager().getCriteriaBuilder().like(Attribute.get("pro_tradeDetails").as(String.class), tradeDetails)));
        try {
            return (product) getEntityManager().createQuery(Query).getSingleResult();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public List<Object[]> findCard(String identification) {
        javax.persistence.criteria.CriteriaQuery Query = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> attributeCard = Query.from(transactions.class);
        Expression<String> idCard = attributeCard.get("tra_product").get("pro_id").as(String.class);
        Expression<String> tradeCard = attributeCard.get("tra_product").get("pro_tradeDetails").as(String.class);
        Query.multiselect(idCard, tradeCard).distinct(true);
        Query.where(getEntityManager().getCriteriaBuilder().equal(attributeCard.get("tra_client").get("cli_identification").as(String.class), identification));
        try {
            return getEntityManager().createQuery(Query).getResultList();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public List<transactions> findTransactions(String identification, Date dateBegin, Date dateEnd) {
        javax.persistence.criteria.CriteriaQuery transactionQuery = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> transactionAttribute = transactionQuery.from(transactions.class);
        transactionQuery.select(transactionAttribute);
        transactionQuery.where(getEntityManager().getCriteriaBuilder().equal(transactionAttribute.get("tra_client").get("cli_identification").as(String.class), identification),
                getEntityManager().getCriteriaBuilder().and(getEntityManager().getCriteriaBuilder().between(transactionAttribute.get("tra_date").as(Date.class), dateBegin, dateEnd)));
        try {
            return getEntityManager().createQuery(transactionQuery).getResultList();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public List<transactions> findTransactionsCustom(String identification, int cardID, Date dateBegin, Date dateEnd) {
        javax.persistence.criteria.CriteriaQuery transactionQuery = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> transactionAttribute = transactionQuery.from(transactions.class);
        transactionQuery.select(transactionAttribute);
        if (cardID == 0) {
            transactionQuery.where(getEntityManager().getCriteriaBuilder().equal(transactionAttribute.get("tra_client").get("cli_identification").as(String.class), identification),
                    getEntityManager().getCriteriaBuilder().and(getEntityManager().getCriteriaBuilder().between(transactionAttribute.get("tra_date").as(Date.class), dateBegin, dateEnd)));
        } else {
            transactionQuery.where(getEntityManager().getCriteriaBuilder().equal(transactionAttribute.get("tra_client").get("cli_identification").as(String.class), identification),
                    getEntityManager().getCriteriaBuilder().and(getEntityManager().getCriteriaBuilder().between(transactionAttribute.get("tra_date").as(Date.class), dateBegin, dateEnd)),
                    getEntityManager().getCriteriaBuilder().and(getEntityManager().getCriteriaBuilder().equal(transactionAttribute.get("tra_product").get("pro_id"), cardID)));
        }
        try {
            return getEntityManager().createQuery(transactionQuery).getResultList();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public List<Object[]> findConsumerCards(String identification, Date dateBegin, Date dateEnd) {
        javax.persistence.criteria.CriteriaQuery transactionQuery = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> transactionAttribute = transactionQuery.from(transactions.class);
        Expression<String> groupByExp = transactionAttribute.get("tra_product").get("pro_tradeDetails").as(String.class);
        Expression<Double> sumExp = getEntityManager().getCriteriaBuilder().sum(transactionAttribute.get("tra_total_value").as(Double.class));
        transactionQuery.multiselect(groupByExp, sumExp);
        transactionQuery.where(getEntityManager().getCriteriaBuilder().equal(transactionAttribute.get("tra_client").get("cli_identification").as(String.class), identification),
                getEntityManager().getCriteriaBuilder().and(getEntityManager().getCriteriaBuilder().between(transactionAttribute.get("tra_date").as(Date.class), dateBegin, dateEnd)));
        transactionQuery.groupBy(groupByExp);
        transactionQuery.orderBy(getEntityManager().getCriteriaBuilder().desc(sumExp));
        try {
            return getEntityManager().createQuery(transactionQuery).setMaxResults(5).getResultList();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }

    }

    public List<Object[]> findConsumerCommerces(String identification, Date dateBegin, Date dateEnd) {
        javax.persistence.criteria.CriteriaQuery transactionQuery = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> transactionAttribute = transactionQuery.from(transactions.class);
        Expression<String> groupByExp = transactionAttribute.get("tra_commerce").get("com_name").as(String.class);
        Expression<Double> sumExp = getEntityManager().getCriteriaBuilder().sum(transactionAttribute.get("tra_total_value").as(Double.class));
        transactionQuery.multiselect(groupByExp, sumExp);
        transactionQuery.where(getEntityManager().getCriteriaBuilder().equal(transactionAttribute.get("tra_client").get("cli_identification").as(String.class), identification),
                getEntityManager().getCriteriaBuilder().and(getEntityManager().getCriteriaBuilder().between(transactionAttribute.get("tra_date").as(Date.class), dateBegin, dateEnd)));
        transactionQuery.groupBy(groupByExp);
        transactionQuery.orderBy(getEntityManager().getCriteriaBuilder().desc(sumExp));
        try {
            return getEntityManager().createQuery(transactionQuery).setMaxResults(5).getResultList();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }

    public List<Object[]> findConsumerCategories(String identification, Date dateBegin, Date dateEnd) {
        javax.persistence.criteria.CriteriaQuery transactionQuery = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> transactionAttribute = transactionQuery.from(transactions.class);
        Expression<String> groupByExp = transactionAttribute.get("tra_commerce").get("commerce_trade").get("cot_name").as(String.class);
        Expression<Double> sumExp = getEntityManager().getCriteriaBuilder().sum(transactionAttribute.get("tra_total_value").as(Double.class));
        transactionQuery.multiselect(groupByExp, sumExp);
        transactionQuery.where(getEntityManager().getCriteriaBuilder().equal(transactionAttribute.get("tra_client").get("cli_identification").as(String.class), identification),
                getEntityManager().getCriteriaBuilder().and(getEntityManager().getCriteriaBuilder().between(transactionAttribute.get("tra_date").as(Date.class), dateBegin, dateEnd)));
        transactionQuery.groupBy(groupByExp);
        transactionQuery.orderBy(getEntityManager().getCriteriaBuilder().desc(sumExp));
        try {
            return getEntityManager().createQuery(transactionQuery).setMaxResults(5).getResultList();
        } catch (Exception ex) {
            LOG.error(ex);
            return null;
        }
    }
}
