/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.services;

import com.ameise.creditcardbalancer.model.transactions;
import com.ameise.creditcardbalancer.util.UtilJWT;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author andres
 */
@Stateless
@Path("transactions")
public class transactionsFacadeREST extends AbstractFacade<transactions> implements Serializable {

    private static final Logger LOG = Logger.getLogger(transactionsFacadeREST.class.getName());

    @PersistenceContext(unitName = "Analytics_PU")
    private EntityManager em;

    @Inject
    private UtilJWT jwt;

    public transactionsFacadeREST() {
        super(transactions.class);
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//      WS for view Comsumer all with creditCard - CDiaz
////////////////////////////////////////////////////////////////////////////////////////////////////
    @POST
    @OPTIONS
    @Path("getComsumerAll")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findComsumerAll(@Context HttpHeaders headers, String clientDATA) throws ParseException {
        LOG.info("Nueva peticion realizada: >>  User-Agent: " + headers.getRequestHeader("user-agent").get(0) + " << >>  Data: " + clientDATA);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject JSONObject = new JSONObject(clientDATA);
        String token = JSONObject.getString("token");
        String identification = JSONObject.getString("identification");
        Date dateBegin = format.parse(JSONObject.getString("dateBegin"));
        Date dateEnd = format.parse(JSONObject.getString("dateEnd"));
        if (jwt.validationToken(token, identification)) {
            JSONArray JSONArray = new JSONArray();
            List<transactions> transtions = super.findTransactions(identification, dateBegin, dateEnd);
            for (transactions transtion : transtions) {
                JSONObject JSONReturn = new JSONObject();
                JSONReturn.put("comercio", transtion.getTra_commerce().getCom_name());
                JSONReturn.put("categoria", transtion.getTra_commerce().getCommerce_trade().getCot_name());
                JSONReturn.put("tarjeta", transtion.getTra_product().getPro_tradeDetails());
                JSONReturn.put("fecha", format.format(transtion.getTra_date()));
                JSONReturn.put("monto", transtion.getTra_total_value());
                JSONArray.put(JSONReturn);
            }
            return Response.ok(JSONArray.toString())
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();

        } else {
            return Response.ok("{\"codResp\":\"1003\",\"msjResp\":\"Error en la validacion de token\"}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//      WS for view Comsumer to creditCard - CDiaz
////////////////////////////////////////////////////////////////////////////////////////////////////
    @POST
    @OPTIONS
    @Path("getConsumerCard")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findConsumerCard(@Context HttpHeaders headers, String clientDATA) throws ParseException {
        System.out.println("\n" + "Nueva peticion realizada\n" + " >>  User-Agent: " + headers.getRequestHeader("user-agent").get(0) + "\n" + " >>  Data: " + clientDATA);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject JSONObject = new JSONObject(clientDATA);
        String token = JSONObject.getString("token");
        String identification = JSONObject.getString("identification");
        Date dateBegin = format.parse(JSONObject.getString("dateBegin"));
        Date dateEnd = format.parse(JSONObject.getString("dateEnd"));
        if (jwt.validationToken(token, identification)) {
            JSONArray JSONArray = new JSONArray();
            List<Object[]> transtions = super.findConsumerCards(identification, dateBegin, dateEnd);
            double totalGlobal = 0;
            for (Object[] transtion : transtions) {
                JSONObject JSONObjecto = new JSONObject();
                JSONObjecto.put("tarjeta", (String) transtion[0]);
                JSONObjecto.put("total", (double) transtion[1]);
                totalGlobal += (double) transtion[1];
                JSONArray.put(JSONObjecto);
            }
            JSONObject JSONReturn = new JSONObject();
            JSONReturn.put("totalGlobal", totalGlobal);
            JSONReturn.put("consumos", JSONArray);
            return Response.ok().entity(JSONReturn.toString())
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        } else {
            return Response.ok("{\"codResp\":\"1003\",\"msjResp\":\"Error en la validacion de token\"}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//      WS for view Comsumer to Commerce - CDiaz
////////////////////////////////////////////////////////////////////////////////////////////////////
    @POST
    @OPTIONS
    @Path("getConsumerCommerce")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findConsumerCommerce(@Context HttpHeaders headers, String clientDATA) throws ParseException {
        System.out.println("\n" + "Nueva peticion realizada\n" + " >>  User-Agent: " + headers.getRequestHeader("user-agent").get(0) + "\n" + " >>  Data: " + clientDATA);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject JSONObject = new JSONObject(clientDATA);
        String token = JSONObject.getString("token");
        String identification = JSONObject.getString("identification");
        Date dateBegin = format.parse(JSONObject.getString("dateBegin"));
        Date dateEnd = format.parse(JSONObject.getString("dateEnd"));
        if (jwt.validationToken(token, identification)) {
            JSONArray JSONArray = new JSONArray();
            List<Object[]> transtions = super.findConsumerCommerces(identification, dateBegin, dateEnd);
            for (Object[] transtion : transtions) {
                JSONObject JSONObjecto = new JSONObject();
                JSONObjecto.put("comercio", (String) transtion[0]);
                JSONObjecto.put("total", (double) transtion[1]);
                JSONArray.put(JSONObjecto);
            }
            return Response.ok().entity(JSONArray.toString())
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        } else {
            return Response.ok("{\"codResp\":\"1003\",\"msjResp\":\"Error en la validacion de token\"}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//      WS for view Comsumer to Commerce trade - CDiaz
////////////////////////////////////////////////////////////////////////////////////////////////////
    @POST
    @OPTIONS
    @Path("getConsumerCategory")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findConsumerCategory(@Context HttpHeaders headers, String clientDATA) throws ParseException {
        System.out.println("\n" + "Nueva peticion realizada\n" + " >>  User-Agent: " + headers.getRequestHeader("user-agent").get(0) + "\n" + " >>  Data: " + clientDATA);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject JSONObject = new JSONObject(clientDATA);
        String token = JSONObject.getString("token");
        String identification = JSONObject.getString("identification");
        Date dateBegin = format.parse(JSONObject.getString("dateBegin"));
        Date dateEnd = format.parse(JSONObject.getString("dateEnd"));
        if (jwt.validationToken(token, identification)) {
            JSONArray JSONArray = new JSONArray();
            List<Object[]> transtions = super.findConsumerCategories(identification, dateBegin, dateEnd);
            for (Object[] transtion : transtions) {
                JSONObject JSONObjecto = new JSONObject();
                JSONObjecto.put("categoria", (String) transtion[0]);
                JSONObjecto.put("total", (double) transtion[1]);
                JSONArray.put(JSONObjecto);
            }
            return Response.ok().entity(JSONArray.toString())
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        } else {
            return Response.ok("{\"codResp\":\"1003\",\"msjResp\":\"Error en la validacion de token\"}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//      WS for view all creadtiCard - Jhanna
////////////////////////////////////////////////////////////////////////////////////////////////////
    @POST
    @OPTIONS
    @Path("getCard")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findCard(@Context HttpHeaders headers, String clientDATA) {
        LOG.info("Nueva peticion realizada: >>  User-Agent: " + headers.getRequestHeader("user-agent").get(0) + " << >>  Data: " + clientDATA);
        LOG.info("Ingreso de nueva peticion para extraer las tarjetas del cliente, findCard[" + clientDATA + "]");
        JSONObject JSONClient = new JSONObject(clientDATA);
        String token = JSONClient.getString("token");
        String identification = JSONClient.getString("identification");
        if (jwt.validationToken(token, identification)) {
            JSONArray JSONArray = new JSONArray();
            List<Object[]> transactions = super.findCard(identification);
            for (Object[] transaccion : transactions) {
                JSONObject JSONObject = new JSONObject();
                JSONObject.put("id", transaccion[0]);
                JSONObject.put("description", transaccion[1]);
                JSONArray.put(JSONObject);
            }
            LOG.info("Datos del metodo findCard[" + clientDATA + "] del WebServices \"getCard\" que van a ser enviados como respuesta: " + JSONArray.toString());
            return Response.ok().entity(JSONArray.toString())
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "GET, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        } else {
            LOG.error("Error en la validacion de token, findCard[" + clientDATA + "]");
            return Response.ok("{\"codResp\":\"1003\",\"msjResp\":\"Error en la validacion de token\"}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        }
    }

////////////////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////////////////
    @POST
    @OPTIONS
    @Path("getComsumer")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findComsumer(@Context HttpHeaders headers, String clientDATA) throws ParseException {
        LOG.info("Nueva peticion realizada: >>  User-Agent: " + headers.getRequestHeader("user-agent").get(0) + " << >>  Data: " + clientDATA);
        LOG.info("Ingreso de nueva peticion para extraer los consumos del las tarjetas del cliente, findComsumer[" + clientDATA + "]");
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        JSONObject JSONObject = new JSONObject(clientDATA);
        String token = JSONObject.getString("token");
        String identification = JSONObject.getString("identification");
        int cardID = JSONObject.getInt("cardID");
        Date dateBegin = format.parse(JSONObject.getString("dateBegin"));
        Date dateEnd = format.parse(JSONObject.getString("dateEnd"));
        if (jwt.validationToken(token, identification)) {
            JSONArray JSONArray = new JSONArray();
            List<transactions> transtions = super.findTransactionsCustom(identification, cardID, dateBegin, dateEnd);
            double totalTrans = 0;
            for (transactions transtion : transtions) {
                JSONObject JSONReturn = new JSONObject();
                JSONReturn.put("id", transtion.getTra_id());
                JSONReturn.put("description", transtion.getTra_commerce().getCom_name());
                JSONReturn.put("ammount", transtion.getTra_total_value());
                JSONReturn.put("category", transtion.getTra_commerce().getCommerce_trade().getCot_name());
                totalTrans += transtion.getTra_total_value();
                JSONArray.put(JSONReturn);
            }
            JSONObject JSONReturn = new JSONObject();
            JSONReturn.put("totalAmmount", totalTrans);
            JSONReturn.put("detail", JSONArray);
            LOG.info("Datos del metodo findComsumer[" + clientDATA + "] del WebServices \"getComsumer\" que van a ser enviados como respuesta: " + JSONReturn.toString());
            return Response.ok(JSONReturn.toString())
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();

        } else {
            LOG.error("Error en la validacion de token, findComsumer[" + clientDATA + "]");
            return Response.ok("{\"codResp\":\"1003\",\"msjResp\":\"Error en la validacion de token\"}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        }
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
