/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ameise.creditcardbalancer.services;

import com.ameise.creditcardbalancer.model.channel;
import com.ameise.creditcardbalancer.model.client;
import com.ameise.creditcardbalancer.model.commerce;
import com.ameise.creditcardbalancer.model.commerce_trade;
import com.ameise.creditcardbalancer.model.product;
import com.ameise.creditcardbalancer.model.transactions;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;
import org.json.JSONObject;

/**
 *
 * @author andres
 */
@Stateless
@Path("create")
public class createDataREST extends AbstractFacade<transactions> {

    private static final Logger LOG = Logger.getLogger(createDataREST.class.getName());

    @PersistenceContext(unitName = "Analytics_PU")
    private EntityManager em;

    public createDataREST() {
        super(transactions.class);
    }

    @POST
    @OPTIONS
    @Path("dataTransactions")
    @Produces({MediaType.APPLICATION_JSON})
    public Response insertData(@Context HttpHeaders headers, transactions trans) {
        JSONObject json = new JSONObject(trans);
        String msjResp;
        LOG.info("Nueva peticion realizada: >>  User-Agent: " + headers.getRequestHeader("user-agent").get(0) + " << >>  Data: " + json.toString());
        try {
            client cli = super.findClient(trans.getTra_client().getCli_identification());
            channel cha = super.findChannel(trans.getTra_channel().getCha_name());
            commerce com = super.findCommerce(trans.getTra_commerce().getCom_name());
            product pro = super.findProduct(trans.getTra_product().getPro_name(), trans.getTra_product().getPro_tradeDetails());
            commerce_trade cot = super.findCommerceTrade(trans.getTra_commerce().getCommerce_trade().getCot_name());
            if (trans.getTra_id() != 0) {
                transactions tra = super.findTransaction(trans.getTra_id());
                tra.setTra_accounting_date(trans.getTra_accounting_date());
                tra.setTra_reference(trans.getTra_reference());
                tra.setTra_description(trans.getTra_description());
                tra.setTra_deferred(trans.getTra_deferred());
                tra.setTra_dues(trans.getTra_dues());
                tra.setTra_interest(trans.getTra_interest());
                tra.setTra_idc(trans.getTra_idc());
                tra.setTra_value(trans.getTra_value());
                tra.setTra_value_interest(trans.getTra_value_interest());
                tra.setTra_value_idc(trans.getTra_value_idc());
                tra.setTra_total_value(trans.getTra_total_value());
                tra.setTra_value_share(trans.getTra_value_share());
                tra.setTra_status(trans.getTra_status());
                if (cli != null) {
                    tra.setTra_client(cli);
                }
                if (cha != null) {
                    tra.setTra_channel(cha);
                }
                if (pro != null) {
                    tra.setTra_product(pro);
                }
                if (com != null) {
                    tra.setTra_commerce(com);
                }
                if (cot != null) {
                    tra.getTra_commerce().setCommerce_trade(cot);
                }
                super.edit(tra);
                msjResp = "\"Transaccion actualizada con exito\",\"codTran\":\"" + tra.getTra_id() + "\"";
                LOG.info("La transaccion a sido actualizada con exito, codigo de la transaccion " + tra.getTra_id());
            } else {
                if (cli != null) {
                    trans.setTra_client(cli);
                }
                if (cha != null) {
                    trans.setTra_channel(cha);
                }
                if (pro != null) {
                    trans.setTra_product(pro);
                }
                if (com != null) {
                    trans.setTra_commerce(com);
                }
                if (cot != null) {
                    trans.getTra_commerce().setCommerce_trade(cot);
                }
                super.create(trans);
                Integer codTran = super.findTransactionID();
                msjResp = "\"Transaccion ingresada con exito\",\"codTran\":\"" + codTran + "\"";
                LOG.info("La transaccion a sido insertada con exito, codigo de la transaccion " + codTran);
            }
            return Response.ok("{\"codResp\":\"0000\",\"msjResp\":" + msjResp + "}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        } catch (Exception ex) {
            LOG.error("Ha ocurrido un error en el ingreso de transaccion: " + json.toString());
            LOG.error(ex);
            return Response.ok("{\"codResp\":\"1111\",\"msjResp\":\"Ha ocurrido un error en el ingreso de transaccion\"}")
                    .header("Access-Control-Allow-Origin", "*")
                    .header("Access-Control-Allow-Methods", "POST, OPTIONS")
                    .header("Access-Control-Allow-Headers", "Content-Type, Accept, X-Requested-With")
                    .build();
        }
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
