package com.ameise.creditcardbalancer.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "TRANSACTIONS", schema = "public")
@XmlRootElement
public class transactions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "TRA_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int tra_id;

    @Column(name = "TRA_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date tra_date;

    @Column(name = "TRA_ACCOUNTING_DATE")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date tra_accounting_date;

    @Column(name = "TRA_REFERENCE", length = 50)
    private String tra_reference;

    @Column(name = "TRA_DESCRIPTION", length = 250)
    private String tra_description;

    @Column(name = "TRA_DEFERRED", length = 3)
    private String tra_deferred;

    @Column(name = "TRA_DUES")
    private int tra_dues;

    @Column(name = "TRA_INTEREST", length = 3)
    private String tra_interest;

    @Column(name = "TRA_IDC", length = 3)
    private String tra_idc;

    @Column(name = "TRA_VALUE")
    private double tra_value;

    @Column(name = "TRA_VALUE_INTEREST")
    private double tra_value_interest;

    @Column(name = "TRA_VALUE_IDC")
    private double tra_value_idc;

    @Column(name = "TRA_TOTAL_VALUE")
    private double tra_total_value;

    @Column(name = "TRA_VALUE_SHARE")
    private double tra_value_share;

    @Column(name = "TRA_STATUS", length = 3)
    private String tra_status;

    @JoinColumn(name = "CHA_ID")
    @ManyToOne(cascade = {CascadeType.ALL}, targetEntity = channel.class, optional = false)
    private channel tra_channel;

    @JoinColumn(name = "COM_ID")
    @ManyToOne(cascade = {CascadeType.ALL}, targetEntity = commerce.class, optional = false)
    private commerce tra_commerce;

    @JoinColumn(name = "CLI_ID")
    @ManyToOne(cascade = {CascadeType.ALL}, targetEntity = client.class, optional = false)
    private client tra_client;

    @JoinColumn(name = "PRO_ID")
    @ManyToOne(cascade = {CascadeType.ALL}, targetEntity = product.class, optional = false)
    private product tra_product;

    public int getTra_id() {
        return tra_id;
    }

    public void setTra_id(int tra_id) {
        this.tra_id = tra_id;
    }

    public Date getTra_date() {
        return tra_date;
    }

    public void setTra_date(Date tra_date) {
        this.tra_date = tra_date;
    }

    public Date getTra_accounting_date() {
        return tra_accounting_date;
    }

    public void setTra_accounting_date(Date tra_accounting_date) {
        this.tra_accounting_date = tra_accounting_date;
    }

    public String getTra_reference() {
        return tra_reference;
    }

    public void setTra_reference(String tra_reference) {
        this.tra_reference = tra_reference;
    }

    public String getTra_description() {
        return tra_description;
    }

    public void setTra_description(String tra_description) {
        this.tra_description = tra_description;
    }

    public String getTra_deferred() {
        return tra_deferred;
    }

    public void setTra_deferred(String tra_deferred) {
        this.tra_deferred = tra_deferred;
    }

    public int getTra_dues() {
        return tra_dues;
    }

    public void setTra_dues(int tra_dues) {
        this.tra_dues = tra_dues;
    }

    public String getTra_interest() {
        return tra_interest;
    }

    public void setTra_interest(String tra_interest) {
        this.tra_interest = tra_interest;
    }

    public String getTra_idc() {
        return tra_idc;
    }

    public void setTra_idc(String tra_idc) {
        this.tra_idc = tra_idc;
    }

    public double getTra_value() {
        return tra_value;
    }

    public void setTra_value(double tra_value) {
        this.tra_value = tra_value;
    }

    public double getTra_value_interest() {
        return tra_value_interest;
    }

    public void setTra_value_interest(double tra_value_interest) {
        this.tra_value_interest = tra_value_interest;
    }

    public double getTra_value_idc() {
        return tra_value_idc;
    }

    public void setTra_value_idc(double tra_value_idc) {
        this.tra_value_idc = tra_value_idc;
    }

    public double getTra_total_value() {
        return tra_total_value;
    }

    public void setTra_total_value(double tra_total_value) {
        this.tra_total_value = tra_total_value;
    }

    public double getTra_value_share() {
        return tra_value_share;
    }

    public void setTra_value_share(double tra_value_share) {
        this.tra_value_share = tra_value_share;
    }

    public String getTra_status() {
        return tra_status;
    }

    public void setTra_status(String tra_status) {
        this.tra_status = tra_status;
    }

    public channel getTra_channel() {
        return tra_channel;
    }

    public void setTra_channel(channel tra_channel) {
        this.tra_channel = tra_channel;
    }

    public commerce getTra_commerce() {
        return tra_commerce;
    }

    public void setTra_commerce(commerce tra_commerce) {
        this.tra_commerce = tra_commerce;
    }

    public client getTra_client() {
        return tra_client;
    }

    public void setTra_client(client tra_client) {
        this.tra_client = tra_client;
    }

    public product getTra_product() {
        return tra_product;
    }

    public void setTra_product(product tra_product) {
        this.tra_product = tra_product;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.tra_id;
        hash = 97 * hash + Objects.hashCode(this.tra_date);
        hash = 97 * hash + Objects.hashCode(this.tra_accounting_date);
        hash = 97 * hash + Objects.hashCode(this.tra_reference);
        hash = 97 * hash + Objects.hashCode(this.tra_description);
        hash = 97 * hash + Objects.hashCode(this.tra_deferred);
        hash = 97 * hash + this.tra_dues;
        hash = 97 * hash + Objects.hashCode(this.tra_interest);
        hash = 97 * hash + Objects.hashCode(this.tra_idc);
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.tra_value) ^ (Double.doubleToLongBits(this.tra_value) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.tra_value_interest) ^ (Double.doubleToLongBits(this.tra_value_interest) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.tra_value_idc) ^ (Double.doubleToLongBits(this.tra_value_idc) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.tra_total_value) ^ (Double.doubleToLongBits(this.tra_total_value) >>> 32));
        hash = 97 * hash + (int) (Double.doubleToLongBits(this.tra_value_share) ^ (Double.doubleToLongBits(this.tra_value_share) >>> 32));
        hash = 97 * hash + Objects.hashCode(this.tra_status);
        hash = 97 * hash + Objects.hashCode(this.tra_channel);
        hash = 97 * hash + Objects.hashCode(this.tra_commerce);
        hash = 97 * hash + Objects.hashCode(this.tra_client);
        hash = 97 * hash + Objects.hashCode(this.tra_product);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final transactions other = (transactions) obj;
        if (this.tra_id != other.tra_id) {
            return false;
        }
        if (this.tra_dues != other.tra_dues) {
            return false;
        }
        if (Double.doubleToLongBits(this.tra_value) != Double.doubleToLongBits(other.tra_value)) {
            return false;
        }
        if (Double.doubleToLongBits(this.tra_value_interest) != Double.doubleToLongBits(other.tra_value_interest)) {
            return false;
        }
        if (Double.doubleToLongBits(this.tra_value_idc) != Double.doubleToLongBits(other.tra_value_idc)) {
            return false;
        }
        if (Double.doubleToLongBits(this.tra_total_value) != Double.doubleToLongBits(other.tra_total_value)) {
            return false;
        }
        if (Double.doubleToLongBits(this.tra_value_share) != Double.doubleToLongBits(other.tra_value_share)) {
            return false;
        }
        if (!Objects.equals(this.tra_reference, other.tra_reference)) {
            return false;
        }
        if (!Objects.equals(this.tra_description, other.tra_description)) {
            return false;
        }
        if (!Objects.equals(this.tra_deferred, other.tra_deferred)) {
            return false;
        }
        if (!Objects.equals(this.tra_interest, other.tra_interest)) {
            return false;
        }
        if (!Objects.equals(this.tra_idc, other.tra_idc)) {
            return false;
        }
        if (!Objects.equals(this.tra_status, other.tra_status)) {
            return false;
        }
        if (!Objects.equals(this.tra_date, other.tra_date)) {
            return false;
        }
        if (!Objects.equals(this.tra_accounting_date, other.tra_accounting_date)) {
            return false;
        }
        if (!Objects.equals(this.tra_channel, other.tra_channel)) {
            return false;
        }
        if (!Objects.equals(this.tra_commerce, other.tra_commerce)) {
            return false;
        }
        if (!Objects.equals(this.tra_client, other.tra_client)) {
            return false;
        }
        if (!Objects.equals(this.tra_product, other.tra_product)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "transactions{" + "tra_id=" + tra_id + ", tra_date=" + tra_date + ", tra_accounting_date=" + tra_accounting_date + ", tra_reference=" + tra_reference + ", tra_description=" + tra_description + ", tra_deferred=" + tra_deferred + ", tra_dues=" + tra_dues + ", tra_interest=" + tra_interest + ", tra_idc=" + tra_idc + ", tra_value=" + tra_value + ", tra_value_interest=" + tra_value_interest + ", tra_value_idc=" + tra_value_idc + ", tra_total_value=" + tra_total_value + ", tra_value_share=" + tra_value_share + ", tra_status=" + tra_status + ", tra_channel=" + tra_channel + ", tra_commerce=" + tra_commerce + ", tra_client=" + tra_client + ", tra_product=" + tra_product + '}';
    }

}
