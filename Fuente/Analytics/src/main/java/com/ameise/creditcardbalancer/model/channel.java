package com.ameise.creditcardbalancer.model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "CHANNEL", schema = "public")
@XmlRootElement
public class channel implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "CHA_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int cha_id;

    @Column(name = "CHA_NAME", length = 50)
    private String cha_name;

    @Column(name = "CHA_DESCRIPTION", length = 250)
    private String cha_description;

    @Column(name = "CHA_STATUS", length = 3)
    private String cha_status;

    public int getCha_id() {
        return cha_id;
    }

    public void setCha_id(int cha_id) {
        this.cha_id = cha_id;
    }

    public String getCha_name() {
        return cha_name;
    }

    public void setCha_name(String cha_name) {
        this.cha_name = cha_name;
    }

    public String getCha_description() {
        return cha_description;
    }

    public void setCha_description(String cha_description) {
        this.cha_description = cha_description;
    }

    public String getCha_status() {
        return cha_status;
    }

    public void setCha_status(String cha_status) {
        this.cha_status = cha_status;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.cha_id;
        hash = 53 * hash + Objects.hashCode(this.cha_name);
        hash = 53 * hash + Objects.hashCode(this.cha_description);
        hash = 53 * hash + Objects.hashCode(this.cha_status);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final channel other = (channel) obj;
        if (this.cha_id != other.cha_id) {
            return false;
        }
        if (!Objects.equals(this.cha_name, other.cha_name)) {
            return false;
        }
        if (!Objects.equals(this.cha_description, other.cha_description)) {
            return false;
        }
        return Objects.equals(this.cha_status, other.cha_status);
    }

    @Override
    public String toString() {
        return "channel{" + "cha_id=" + cha_id + ", cha_name=" + cha_name + ", cha_description=" + cha_description + ", cha_status=" + cha_status + '}';
    }

}
